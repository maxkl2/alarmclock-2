# AlarmClock 2

Second attempt at a standalone alarm clock which includes a sunrise-simulating light. The previous one can be found at [maxkl2/alarmclock](https://gitlab.com/maxkl2/alarmclock).

## Sunrise light

![](KiCad/SunriseLight/3DRender-Front.png)

![](KiCad/SunriseLight/3DRender-Back.png)
