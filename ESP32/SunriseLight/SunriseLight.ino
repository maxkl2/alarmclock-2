
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include "src/common/LedControl.h"
#include "src/common/fading.h"

#define UUID_SERVICE "c2319101-1e43-4960-a57b-cb2e3bc6893d"
// Characteristic "Sync Request": Notification
#define UUID_CHAR_SYNC_REQUEST "89ea188e-9a04-4e35-b349-afacad50dce2"
// Characteristic "Manual": U16 colorTemp, U8 brightness
#define UUID_CHAR_MANUAL "93e4ddcc-d0ff-45bf-883d-4dd674ec83a7"
// Characteristic "Sunrise": I24 delay, U16 duration, U16 colorTemp, U8 brightness
#define UUID_CHAR_SUNRISE "52bf3022-e0a1-4cc8-a72f-99ae45fc8cb0"
// Characteristic "Sunset": I24 delay, U16 duration, U16 colorTemp, U8 brightness
#define UUID_CHAR_SUNSET "736e6e70-1742-4a9b-95e4-8ecebf399fdb"
// Characteristic "Temp": Notification; I16 temp5000K, I16 temp2700K
#define UUID_CHAR_TEMP "6c1eef48-ff7c-4a7e-ba04-400e1dfe3c4e"

// Use the LED on the ESP32 board to indicate whether BLE is connected
#define PIN_LED_BLE 2
#define PIN_NTC_5000K 39
#define PIN_NTC_2700K 36

BLEServer *bleServer = NULL;
BLECharacteristic bleCharSyncRequest(UUID_CHAR_SYNC_REQUEST, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
BLECharacteristic bleCharManual(UUID_CHAR_MANUAL, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
BLECharacteristic bleCharSunrise(UUID_CHAR_SUNRISE, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
BLECharacteristic bleCharSunset(UUID_CHAR_SUNSET, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);
BLECharacteristic bleCharTemp(UUID_CHAR_TEMP, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);

bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;

class MyServerCallbacks : public BLEServerCallbacks {
    void onConnect(BLEServer *bleServer) {
        deviceConnected = true;

        digitalWrite(PIN_LED_BLE, HIGH);
    };

    void onDisconnect(BLEServer *bleServer) {
        deviceConnected = false;

        digitalWrite(PIN_LED_BLE, LOW);
    }
};

static uint16_t bytes_to_u16_be(const uint8_t *bytes) {
    return bytes[0] << 8 | bytes[1];
}

static uint32_t bytes_to_u24_be(const uint8_t *bytes) {
    return bytes[0] << 16 | bytes[1] << 8 | bytes[2];
}

static int32_t bytes_to_i24_be(const uint8_t *bytes) {
    uint32_t uns = bytes_to_u24_be(bytes);
    uint32_t mask = 1U << (24 - 1);
    int32_t sgn = (uns ^ mask) - mask;
    return sgn;
}

// static void u16_to_bytes_be(uint16_t v, uint8_t *bytes) {
//     bytes[0] = (v >> 8) & 0xff;
//     bytes[1] = v & 0xff;
// }

// static void i16_to_bytes_be(int16_t v, uint8_t *bytes) {
//     u16_to_bytes_be((uint16_t) v, bytes);
// }

class ManualCallbacks : public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
        std::string value = pCharacteristic->getValue();
        uint8_t *data = (uint8_t *) value.data();
        size_t len = value.length();

        if (len != 3) {
            Serial.print("BLE: Manual: Bad data length: ");
            Serial.print(len);
            Serial.println();
            return;
        }

        uint16_t colorTemp = bytes_to_u16_be(&data[0]);
        uint8_t brightness = data[2];
        Serial.print("Manually set to ");
        Serial.print(colorTemp);
        Serial.print(" K, ");
        Serial.print(((int) brightness * 100) / 255);
        Serial.println(" %");

        fading_set_smooth(colorTemp, (float) brightness / 255.f);
    }
};

class SunriseCallbacks : public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
        std::string value = pCharacteristic->getValue();
        uint8_t *data = (uint8_t *) value.data();
        size_t len = value.length();

        if (len != 8) {
            Serial.print("BLE: Sunrise: Bad data length: ");
            Serial.print(len);
            Serial.println();
            return;
        }

        int32_t delay = bytes_to_i24_be(&data[0]);
        uint16_t duration = bytes_to_u16_be(&data[3]);
        uint16_t colorTemp = bytes_to_u16_be(&data[5]);
        uint8_t brightness = data[7];

        Serial.print("Sunrise in ");
        Serial.print(delay);
        Serial.print(" s, ");
        Serial.print(duration);
        Serial.print(" s long, ");
        Serial.print(colorTemp);
        Serial.print(" K, ");
        Serial.print(((int) brightness * 100) / 255);
        Serial.println(" %");

        fading_schedule(
            delay,
            duration,
            500.0f,
            colorTemp,
            0.0f,
            (float) brightness / 255
        );
    }
};

class SunsetCallbacks : public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
        std::string value = pCharacteristic->getValue();
        uint8_t *data = (uint8_t *) value.data();
        size_t len = value.length();

        if (len != 8) {
            Serial.print("BLE: Sunset: Bad data length: ");
            Serial.print(len);
            Serial.println();
            return;
        }

        int32_t delay = bytes_to_i24_be(&data[0]);
        uint16_t duration = bytes_to_u16_be(&data[3]);
        uint16_t colorTemp = bytes_to_u16_be(&data[5]);
        uint8_t brightness = data[7];

        Serial.print("Sunset in ");
        Serial.print(delay);
        Serial.print(" s, ");
        Serial.print(duration);
        Serial.print(" s long, ");
        Serial.print(colorTemp);
        Serial.print(" K, ");
        Serial.print(((int) brightness * 100) / 255);
        Serial.println(" %");

        fading_schedule(
            delay,
            duration,
            colorTemp,
            500.0f,
            (float) brightness / 255,
            0.0f
        );
    }
};


void setup() {
    Serial.begin(115200);

    pinMode(PIN_LED_BLE, OUTPUT);
    digitalWrite(PIN_LED_BLE, LOW);

    analogSetPinAttenuation(PIN_NTC_5000K, ADC_11db);
    analogSetPinAttenuation(PIN_NTC_2700K, ADC_11db);

    led_control_init();

    BLEDevice::init("Sunrise Light");

    bleServer = BLEDevice::createServer();
    bleServer->setCallbacks(new MyServerCallbacks());

    BLEService *pService = bleServer->createService(UUID_SERVICE);

    pService->addCharacteristic(&bleCharSyncRequest);
    bleCharSyncRequest.addDescriptor(new BLE2902());

    pService->addCharacteristic(&bleCharManual);
    bleCharManual.setCallbacks(new ManualCallbacks());

    pService->addCharacteristic(&bleCharSunrise);
    bleCharSunrise.setCallbacks(new SunriseCallbacks());

    pService->addCharacteristic(&bleCharSunset);
    bleCharSunset.setCallbacks(new SunsetCallbacks());

    pService->addCharacteristic(&bleCharTemp);
    bleCharTemp.addDescriptor(new BLE2902());

    pService->start();

    BLEAdvertising *ad = bleServer->getAdvertising();
    ad->addServiceUUID(UUID_SERVICE);
    bleServer->startAdvertising();
    Serial.println("BLE: Started advertising");

    fading_task_start();
}

static String cmdbuf;

static float temp = 2700.f;
static float brightness = 0.f;

void processCommand() {
    switch (cmdbuf[0]) {
        case 'o':
            led_set_5000K(0);
            led_set_2700K(0);
            led_set_red(0);
            break;
        case 'c':
            led_set_5000K(cmdbuf.substring(1).toInt());
            break;
        case 'w':
            led_set_2700K(cmdbuf.substring(1).toInt());
            break;
        case 'r':
            led_set_red(cmdbuf.substring(1).toInt());
            break;
        case 't':
            temp = cmdbuf.substring(1).toFloat();
            fading_set_smooth(temp, brightness);
            break;
        case 'b':
            brightness = cmdbuf.substring(1).toFloat();
            fading_set_smooth(temp, brightness);
            break;
        // case 'f':
        //     portENTER_CRITICAL(&next_fade_mux);
        //     next_fade.duration = 1000 * 10;
        //     next_fade.temp_start = 500.0f;
        //     next_fade.temp_end = 4000.0f;
        //     next_fade.brightness_start = 0.0f;
        //     next_fade.brightness_end = 0.8f;
        //     next_fade_start_mode = FADE_START_NORMAL;
        //     portEXIT_CRITICAL(&next_fade_mux);

        //     notify_led_control_task(NOTIFY_FADE_NOW);
        //     break;
        case 's':
            fading_schedule(
                cmdbuf.substring(1).toInt(),
                60,
                500.0f,
                4000.0f,
                0.0f,
                0.8f
            );
            break;
        default:
            Serial.println("Invalid command");
    }
    cmdbuf = "";
}

float calc_temp_ntc(uint16_t n) {
    float v_adc_max = 3.9f;
    float v_adc = v_adc_max * (float) n / 4095.0f;

    // Account for amplification
    float A = 5.0f;
    float v_ntc = v_adc / A;

    // Voltage divider
    float v_div_high = 1.03f;
    float R2 = 10e3f;
    float R = R2 / (v_div_high / v_ntc - 1.0f);

    // NTC equation: R = R0 * e^(B * (1/T - 1/T0)) -> T = T0 * B / (B + T0 * ln(R/R0))
    float T0 = 273.15f + 25.0f;
    float R0 = 10e3f;
    float B = 3434.0f;
    float T = T0 * B / (B + T0 * logf(R / R0));

    // K to °C
    return T - 273.15;
}

float calc_temp_led(float temp_ntc, float current) {
    // Linear approximation from V_f graph
    float v_f = 2.58 + 312.5e-3 * current;
    float p_el = v_f * current;
    // Electrical power is partially converted to optical and electrical power
    // Assume 50 % efficiency
    float eff = 0.5;
    float p_th = (1.0 - eff) * p_el;
    // Assumes that the NTC has the same temperature as the LED's solder pad, which should be about right since they
    //  are close together and the NTC dissipates almost no heat
    float r_th_led = 3.0; // °C/W
    float t_j = temp_ntc + p_th * r_th_led;
    return t_j;
}

void loop() {
    int c;
    while ((c = Serial.read()) != -1) {
        if (c == '\n') {
            processCommand();
        } else {
            cmdbuf += (char) c;
        }
    }

    if (deviceConnected) {
        // bleCharSyncRequest.setValue(&txValue, 1);
        // bleCharSyncRequest.notify();
        // txValue++;
        // delay(10); // bluetooth stack will go into congestion, if too many packets are sent
    }

    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
        Serial.println("BLE: Disconnected");
        delay(500); // give the bluetooth stack the chance to get things ready
        bleServer->startAdvertising(); // restart advertising
        Serial.println("BLE: Started advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
        // do stuff here on connecting
        oldDeviceConnected = deviceConnected;

        Serial.println("BLE: Connected");
    }

    delay(10);
}
