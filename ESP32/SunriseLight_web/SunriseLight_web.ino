
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WebServer.h>

#include "src/common/LedControl.h"
#include "src/common/fading.h"
#include "src/common/temperature.h"

#define PIN_LED 2
#define PIN_EXT_TRIGGER 15

#define RETRIGGER_DELAY 5000 // ms

#include "wifi_credentials.h"


static void IRAM_ATTR isr_ext_trigger(void);

WebServer server(80);

void setup() {
    Serial.begin(115200);

    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, HIGH);

    pinMode(PIN_EXT_TRIGGER, INPUT);

    led_control_init();

    // temp_init();

    attachInterrupt(PIN_EXT_TRIGGER, isr_ext_trigger, RISING);

    Serial.print("WiFi: connecting to ");
    Serial.println(WIFI_SSID);

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi: connected");
    Serial.print("WiFi: got IP: ");
    Serial.println(WiFi.localIP());

    if (!MDNS.begin("sunrise")) {
        Serial.println("mDNS: error setting up responder");
    } else {
        Serial.println("mDNS: responder started");
    }

    server.on("/api/manual", handle_api_manual);
    server.on("/api/sunrise", handle_api_sunrise);
    server.on("/api/sunset", handle_api_sunset);
    server.on("/api/ext_trigger", handle_api_ext_trigger);
    // server.on("/api/temp", handle_api_temp);
    server.onNotFound(handle_NotFound);

    server.begin();
    Serial.println("HTTP: server started");

    MDNS.addService("http", "tcp", 80);

    fading_task_start();

    digitalWrite(PIN_LED, LOW);
}

static volatile bool triggered = false;
static bool ext_trigger_enabled = true;
static FadeInfo trigger_fade = {
    .start_mode = FADE_START_NORMAL,
    .duration = 180 * 1000,
    .temp_start = 500.0f,
    .temp_end = 5000.0f,
    .brightness_start = 0.0f,
    .brightness_end = 1.0f
};

static void IRAM_ATTR isr_ext_trigger(void) {
    static unsigned long last_trigger = 0;

    unsigned long now = millis();

    // No trigger for at least the last RETRIGGER_DELAY ms
    if (now - last_trigger > RETRIGGER_DELAY) {
        triggered = true;
    }

    last_trigger = now;
}

void loop() {
    if (triggered) {
        triggered = false;

        if (ext_trigger_enabled) {
            Serial.println("External trigger -> starting fade");

            fading_schedule(0, &trigger_fade);
        }
    }

    // temp_loop();

    server.handleClient();
}

void handle_api_manual() {
    uint16_t colorTemp = 2700;
    float brightness = 0.0;

    for (unsigned i = 0; i < server.args(); i++) {
        if (server.argName(i) == "t") {
            String temp_str = server.arg(i);
            long tmp = temp_str.toInt();
            if (tmp >= 500 && tmp <= 5000) {
                colorTemp = tmp;
            }
        }
        if (server.argName(i) == "b") {
            String brightness_str = server.arg(i);
            float tmp = brightness_str.toFloat();
            if (tmp >= 0.0 && tmp <= 1.0) {
                brightness = tmp;
            }
        }
    }

    Serial.print("API: manual: ");
    Serial.print(colorTemp);
    Serial.print(" K, ");
    Serial.print(brightness * 100.0);
    Serial.println(" %");

    fading_set_smooth(colorTemp, brightness);

    server.send(200, "application/json", "{\"result\":0}");
}

void handle_api_sunrise() {
    int32_t delay = 0;
    uint16_t duration = 180;
    uint16_t colorTemp = 2700;
    float brightness = 0.0;

    for (unsigned i = 0; i < server.args(); i++) {
        if (server.argName(i) == "s") {
            String delay_str = server.arg(i);
            long tmp = delay_str.toInt();
            delay = tmp;
        }
        if (server.argName(i) == "d") {
            String duration_str = server.arg(i);
            long tmp = duration_str.toInt();
            if (tmp > 0) {
                duration = tmp;
            }
        }
        if (server.argName(i) == "t") {
            String temp_str = server.arg(i);
            long tmp = temp_str.toInt();
            if (tmp >= 500 && tmp <= 5000) {
                colorTemp = tmp;
            }
        }
        if (server.argName(i) == "b") {
            String brightness_str = server.arg(i);
            float tmp = brightness_str.toFloat();
            if (tmp >= 0.0 && tmp <= 1.0) {
                brightness = tmp;
            }
        }
    }

    Serial.print("API: sunrise: ");
    Serial.print(delay);
    Serial.print(" s, ");
    Serial.print(duration);
    Serial.print(" s, ");
    Serial.print(colorTemp);
    Serial.print(" K, ");
    Serial.print(brightness * 100.0);
    Serial.println(" %");

    FadeInfo fade = {
        .start_mode = FADE_START_NORMAL,
        .duration = duration * 1000,
        .temp_start = 500.0f,
        .temp_end = colorTemp,
        .brightness_start = 0.0f,
        .brightness_end = brightness
    };

    fading_schedule(delay, &fade);

    server.send(200, "application/json", "{\"result\":0}");
}

void handle_api_sunset() {
    int32_t delay = 0;
    uint16_t duration = 180;
    uint16_t colorTemp = 2700;
    float brightness = 0.0;

    for (unsigned i = 0; i < server.args(); i++) {
        if (server.argName(i) == "s") {
            String delay_str = server.arg(i);
            long tmp = delay_str.toInt();
            delay = tmp;
        }
        if (server.argName(i) == "d") {
            String duration_str = server.arg(i);
            long tmp = duration_str.toInt();
            if (tmp > 0) {
                duration = tmp;
            }
        }
        if (server.argName(i) == "t") {
            String temp_str = server.arg(i);
            long tmp = temp_str.toInt();
            if (tmp >= 500 && tmp <= 5000) {
                colorTemp = tmp;
            }
        }
        if (server.argName(i) == "b") {
            String brightness_str = server.arg(i);
            float tmp = brightness_str.toFloat();
            if (tmp >= 0.0 && tmp <= 1.0) {
                brightness = tmp;
            }
        }
    }

    Serial.print("API: sunset: ");
    Serial.print(delay);
    Serial.print(" s, ");
    Serial.print(duration);
    Serial.print(" s, ");
    Serial.print(colorTemp);
    Serial.print(" K, ");
    Serial.print(brightness * 100.0);
    Serial.println(" %");

    FadeInfo fade = {
        .start_mode = FADE_START_SMART,
        .duration = duration * 1000,
        .temp_start = colorTemp,
        .temp_end = 500.0f,
        .brightness_start = brightness,
        .brightness_end = 0.0f
    };

    fading_schedule(delay, &fade);

    server.send(200, "application/json", "{\"result\":0}");
}

void handle_api_ext_trigger() {
    bool enabled = false;
    uint16_t duration = 180;
    uint16_t colorTemp = 2700;
    float brightness = 0.0;

    for (unsigned i = 0; i < server.args(); i++) {
        if (server.argName(i) == "e") {
            String enabled_str = server.arg(i);
            long tmp = enabled_str.toInt();
            enabled = tmp;
        }
        if (server.argName(i) == "d") {
            String duration_str = server.arg(i);
            long tmp = duration_str.toInt();
            if (tmp > 0) {
                duration = tmp;
            }
        }
        if (server.argName(i) == "t") {
            String temp_str = server.arg(i);
            long tmp = temp_str.toInt();
            if (tmp >= 500 && tmp <= 5000) {
                colorTemp = tmp;
            }
        }
        if (server.argName(i) == "b") {
            String brightness_str = server.arg(i);
            float tmp = brightness_str.toFloat();
            if (tmp >= 0.0 && tmp <= 1.0) {
                brightness = tmp;
            }
        }
    }

    Serial.print("API: ext_trigger: ");
    Serial.print(enabled ? "ON" : "OFF");
    Serial.print(", ");
    Serial.print(duration);
    Serial.print(" s, ");
    Serial.print(colorTemp);
    Serial.print(" K, ");
    Serial.print(brightness * 100.0);
    Serial.println(" %");

    ext_trigger_enabled = enabled;

    trigger_fade.start_mode = FADE_START_NORMAL;
    trigger_fade.duration = duration * 1000;
    trigger_fade.temp_start = 500.0f;
    trigger_fade.temp_end = colorTemp;
    trigger_fade.brightness_start = 0.0f;
    trigger_fade.brightness_end = brightness;

    server.send(200, "application/json", "{\"result\":0}");
}

void handle_api_temp() {
    Serial.println("API: temp");

    float temp_2700K_led, temp_2700K_ntc, current_2700K;
    temp_get_2700K(&temp_2700K_led, &temp_2700K_ntc, &current_2700K);

    float temp_5000K_led, temp_5000K_ntc, current_5000K;
    temp_get_5000K(&temp_5000K_led, &temp_5000K_ntc, &current_5000K);

    int temp_2700K_led_i = (int) roundf(temp_2700K_led * 10);
    int temp_2700K_ntc_i = (int) roundf(temp_2700K_ntc * 10);
    int current_2700K_i = (int) roundf(current_2700K * 1000);

    int temp_5000K_led_i = (int) roundf(temp_5000K_led * 10);
    int temp_5000K_ntc_i = (int) roundf(temp_5000K_ntc * 10);
    int current_5000K_i = (int) roundf(current_5000K * 1000);

#define TEMP_BUF_LEN 200
    static char buf[TEMP_BUF_LEN];
    snprintf(
        buf, TEMP_BUF_LEN,
        "{\"result\":0,\"led_2700K\":%i,\"ntc_2700K\":%i,\"i_2700K\":%i,\"led_5000K\":%i,\"ntc_5000K\":%i,\"i_5000K\":%i}",
        temp_2700K_led_i, temp_2700K_ntc_i, current_2700K_i, temp_5000K_led_i, temp_5000K_ntc_i, current_5000K_i
    );

    server.send(200, "application/json", buf);
}

void handle_NotFound() {
    server.send(404, "text/plain", "Not found");
}
