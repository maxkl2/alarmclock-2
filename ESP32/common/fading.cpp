
#include "fading.h"

#include <Arduino.h>

#include "LedControl.h"


#define NOTIFY_FADE_NOW (1 << 0)
#define NOTIFY_FADE_SCHEDULE (1 << 1)
#define NOTIFY_TIMER (1 << 3)

static TaskHandle_t fading_task;

static portMUX_TYPE next_fade_mux = portMUX_INITIALIZER_UNLOCKED;
static FadeInfo next_fade;

static portMUX_TYPE scheduled_fade_mux = portMUX_INITIALIZER_UNLOCKED;
static FadeInfo scheduled_fade;
static int32_t scheduled_fade_delay;

static void fading_task_fn(void *param);

void fading_task_start() {
    xTaskCreatePinnedToCore(
        fading_task_fn,
        "fading",
        10000, // Stack size
        NULL, // Parameter
        0, // Priority
        &fading_task,
        0 // Core
    );
}

static void notify_fading_task(uint32_t bits) {
    xTaskNotify(fading_task, bits, eSetBits);
}

static void notify_fading_task_from_isr(uint32_t bits) {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xTaskNotifyFromISR(fading_task, bits, eSetBits, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void fading_set_smooth(float temp, float brightness) {
    portENTER_CRITICAL(&next_fade_mux);
    next_fade.start_mode = FADE_START_SMART;
    next_fade.duration = 500;
    next_fade.temp_end = temp;
    next_fade.brightness_end = brightness;
    portEXIT_CRITICAL(&next_fade_mux);

    notify_fading_task(NOTIFY_FADE_NOW);
}

void fading_schedule(int32_t delay, FadeInfo *fade) {
    portENTER_CRITICAL(&scheduled_fade_mux);
    scheduled_fade = *fade;
    scheduled_fade_delay = delay;
    portEXIT_CRITICAL(&scheduled_fade_mux);

    notify_fading_task(NOTIFY_FADE_SCHEDULE);
}

static portMUX_TYPE timer_mux = portMUX_INITIALIZER_UNLOCKED;
static volatile uint32_t timer_seconds = 0;

static void ARDUINO_ISR_ATTR timer_handler() {
    portENTER_CRITICAL_ISR(&timer_mux);
    timer_seconds++;
    portEXIT_CRITICAL_ISR(&timer_mux);

    notify_fading_task_from_isr(NOTIFY_TIMER);
}

static void apply_start_mode(FadeInfo *target, FadeInfo *source, float current_temp, float current_brightness) {
    switch (source->start_mode) {
        case FADE_START_CURRENT:
            target->temp_start = current_temp;
            target->brightness_start = current_brightness;
            break;
        case FADE_START_SMART:
            target->temp_start = current_brightness < 0.01f ? source->temp_end : current_temp;
            target->brightness_start = current_brightness;
            break;
        case FADE_START_NORMAL:
        default:
            target->temp_start = source->temp_start;
            target->brightness_start = source->brightness_start;
    }
}

static void fading_task_fn(void *param) {
    float current_temp = 2700.0f;
    float current_brightness = 0.0f;

    bool fade_active = false;
    FadeInfo current_fade;
    uint32_t fade_start_millis;

    bool reset_seconds = false;
    uint32_t seconds = 0;

    bool fade_scheduled = false;
    FadeInfo scheduled_fade_local;
    int32_t scheduled_fade_delay_local;

    // uint32_t next_temp_millis = 0;

    // Set timer frequency to 1 MHz -> timer counts microseconds
    hw_timer_t *timer = timerBegin(1000000);
    timerAttachInterrupt(timer, &timer_handler);
    // Interrupt every second (1e6 us, autoreload indefinitely)
    timerAlarm(timer, 1000000, true, 0);

    while (1) {
        uint32_t notified_bits;
        if (xTaskNotifyWait(0, 0xffffffff, &notified_bits, 0) == pdTRUE) {
            if (notified_bits & NOTIFY_TIMER) {
                portENTER_CRITICAL(&timer_mux);
                if (reset_seconds) {
                    reset_seconds = false;

                    timer_seconds = 0;
                    seconds = 0;
                } else {
                    seconds = timer_seconds;
                }
                portEXIT_CRITICAL(&timer_mux);

                Serial.print("Time: ");
                Serial.print(seconds);
                Serial.println(" s");

                if (fade_scheduled) {
                    if (seconds >= scheduled_fade_delay_local) {
                        fade_scheduled = false;

                        current_fade = scheduled_fade_local;
                        apply_start_mode(&current_fade, &scheduled_fade_local, current_temp, current_brightness);

                        fade_start_millis = millis();
                        fade_active = true;
                    }
                }
            }

            if (notified_bits & NOTIFY_FADE_SCHEDULE) {
                portENTER_CRITICAL(&scheduled_fade_mux);
                scheduled_fade_local = scheduled_fade;
                scheduled_fade_delay_local = scheduled_fade_delay;
                portEXIT_CRITICAL(&scheduled_fade_mux);

                if (scheduled_fade_delay_local <= 0) {
                    // Fade started in the past -> start immediately
                    fade_scheduled = false;

                    current_fade = scheduled_fade_local;
                    // TODO: this doesn't actually make sense, since the starting temp/brightness aren't known anymore. Override start mode? What about delay=0?
                    apply_start_mode(&current_fade, &scheduled_fade_local, current_temp, current_brightness);

                    // scheduled_fade_delay_local is negative here!
                    fade_start_millis = millis() + scheduled_fade_delay_local * 1000;
                    fade_active = true;
                } else {
                    reset_seconds = true;
                    fade_scheduled = true;
                }
            }

            if (notified_bits & NOTIFY_FADE_NOW) {
                portENTER_CRITICAL(&next_fade_mux);
                current_fade = next_fade;
                apply_start_mode(&current_fade, &next_fade, current_temp, current_brightness);
                portEXIT_CRITICAL(&next_fade_mux);

                fade_start_millis = millis();
                fade_active = true;
            }
        }

        if (fade_active) {
            uint32_t t = millis();
            uint32_t elapsed = t - fade_start_millis;
            if (elapsed >= current_fade.duration) {
                current_temp = current_fade.temp_end;
                current_brightness = current_fade.brightness_end;

                fade_active = false;
            } else {
                float p = (float) elapsed / current_fade.duration;
                current_temp = current_fade.temp_start + (current_fade.temp_end - current_fade.temp_start) * p;
                current_brightness = current_fade.brightness_start + (current_fade.brightness_end - current_fade.brightness_start) * p;
            }
            led_set_temp(current_temp, current_brightness);
        }

        // uint32_t now = millis();
        // if (now >= next_temp_millis) {
        //     next_temp_millis = now + 500;

        //     uint16_t ad_ntc_5000K = analogRead(PIN_NTC_5000K);
        //     uint16_t ad_ntc_2700K = analogRead(PIN_NTC_2700K);

        //     // TODO: average, handle spurious ADC readings?

        //     float t_ntc_5000K = calc_temp_ntc(ad_ntc_5000K);
        //     float duty_5000K = (float) led_get_5000K() / 4095.0;
        //     float t_j_5000K = calc_temp_led(t_ntc_5000K, duty_5000K * 0.5);

        //     float t_ntc_2700K = calc_temp_ntc(ad_ntc_2700K);
        //     float duty_2700K = (float) led_get_2700K() / 4095.0;
        //     float t_j_2700K = calc_temp_led(t_ntc_2700K, duty_2700K * 0.667);

        //     // Signed 16-bit fixed-point with 4 fractional bits
        //     int16_t t_j_5000K_fp = (int16_t) roundf(t_j_5000K * (1 << 4));
        //     int16_t t_j_2700K_fp = (int16_t) roundf(t_j_2700K * (1 << 4));

        //     uint8_t ble_data[4];
        //     i16_to_bytes_be(t_j_5000K_fp, &ble_data[0]);
        //     i16_to_bytes_be(t_j_2700K_fp, &ble_data[2]);
        //     bleCharTemp.setValue(ble_data, sizeof(ble_data));
        //     bleCharTemp.notify();

        //     // TODO: impl limit

        //     Serial.print("NTC: ");
        //     Serial.print(t_ntc_5000K);
        //     Serial.print(" / ");
        //     Serial.print(t_j_5000K);
        //     Serial.print(" (");
        //     Serial.print(ad_ntc_5000K);
        //     Serial.print("), ");
        //     Serial.print(t_ntc_2700K);
        //     Serial.print(" / ");
        //     Serial.print(t_j_2700K);
        //     Serial.print(" (");
        //     Serial.print(ad_ntc_2700K);
        //     Serial.println(")");
        // }

        vTaskDelay(pdMS_TO_TICKS(10));
    }
}
