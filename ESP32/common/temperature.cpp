
#include "temperature.h"

#include <Arduino.h>

#include "LedControl.h"

#define TEMP_LOG 1

#define PIN_NTC_5000K 39
#define PIN_NTC_2700K 36

#define UPDATE_INTERVAL 1000  // ms


static float current_2700K = 0.0f;
static float temp_2700K_ntc = 0.0f;
static float temp_2700K_led = 0.0f;
static float current_5000K = 0.0f;
static float temp_5000K_ntc = 0.0f;
static float temp_5000K_led = 0.0f;

static float calc_temp_ntc(uint16_t n) {
    float v_adc_max = 1.1f;
    float v_adc = v_adc_max * (float) n / 4095.0f;

    // Account for amplification
    float A = 1.67f;
    float v_ntc = v_adc / A;

    // Voltage divider
    float v_div_high = 3.3f;
    float R2 = 56e3f;
    float R = R2 / (v_div_high / v_ntc - 1.0f);

    // NTC equation: R = R0 * e^(B * (1/T - 1/T0)) -> T = B / (B / T0 + ln(R/R0))
    float T0 = 273.15f + 25.0f;
    float R0 = 10e3f;
    // From NCU18XH103F60RB datasheet: B_25/50 = 3380K, B_25/80 = 3428K, B_25/85 = 3434K, B_25/100 = 3455K
    float B = 3428.0f;
    float T = B / (B / T0 + logf(R / R0));

#if TEMP_LOG
    Serial.print("v_adc=");
    Serial.print(v_adc);
    Serial.print(" V, v_ntc=");
    Serial.print(v_ntc);
    Serial.print(" V, R=");
    Serial.print(R);
    Serial.print(" Ohm, T=");
    Serial.print(T);
    Serial.println(" K");
#endif

    // K to °C
    return T - 273.15f;
}

static float calc_temp_led(float temp_ntc, float current) {
    // Linear approximation from V_f graph
    float v_f = 2.58 + 312.5e-3 * current;
    float p_el = v_f * current;
    // Electrical power is partially converted to optical and electrical power
    // Assume 50 % efficiency
    float eff = 0.5;
    float p_th = (1.0 - eff) * p_el;
    // Assumes that the NTC has the same temperature as the LED's solder pad, which should be about right since they
    //  are close together and the NTC dissipates almost no heat
    float r_th_led = 3.0; // °C/W
    float t_j = temp_ntc + p_th * r_th_led;

#if TEMP_LOG
    Serial.print("v_f=");
    Serial.print(v_f);
    Serial.print(" V, p_el=");
    Serial.print(p_el);
    Serial.print(" W, p_th=");
    Serial.print(p_th);
    Serial.print(" W, t_j=");
    Serial.print(t_j);
    Serial.println(" C");
#endif

    return t_j;
}

static float temp_from_adc(uint16_t n_adc, float current) {
    float temp_ntc = calc_temp_ntc(n_adc);
    return calc_temp_led(temp_ntc, current);
}

void temp_get_2700K(float *temp_led, float *temp_ntc, float *current) {
    *temp_led = temp_2700K_led;
    *temp_ntc = temp_2700K_ntc;
    *current = current_2700K;
}

void temp_get_5000K(float *temp_led, float *temp_ntc, float *current) {
    *temp_led = temp_5000K_led;
    *temp_ntc = temp_5000K_ntc;
    *current = current_5000K;
}

void temp_update() {
    // TODO: average, handle spurious ADC readings?
    // TODO: implement limit

    current_2700K = led_get_current_2700K();
    uint16_t n_2700K = analogRead(PIN_NTC_2700K);
#if TEMP_LOG
    Serial.print("Temp 2700K: I=");
    Serial.print(current_2700K);
    Serial.print(" A, n=");
    Serial.print(n_2700K);
    Serial.println();
#endif
    temp_2700K_ntc = calc_temp_ntc(n_2700K);
    temp_2700K_led = calc_temp_led(temp_2700K_ntc, current_2700K);


    current_5000K = led_get_current_5000K();
    int n_5000K = analogRead(PIN_NTC_5000K);
#if TEMP_LOG
    Serial.print("Temp 5000K: I=");
    Serial.print(current_5000K);
    Serial.print(" A, n=");
    Serial.print(n_5000K);
    Serial.println();
#endif
    temp_5000K_ntc = calc_temp_ntc(n_5000K);
    temp_5000K_led = calc_temp_led(temp_5000K_ntc, current_5000K);
}

void temp_loop() {
    static unsigned long last_update = 0;
    
    unsigned long now = millis();
    if (now - last_update >= UPDATE_INTERVAL) {
        last_update = now;
        
        temp_update();
    }
}

void temp_init() {
    analogSetAttenuation(ADC_0db);
    analogReadResolution(12);

#if TEMP_LOG
    Serial.println("Temp initialized");
#endif
}
