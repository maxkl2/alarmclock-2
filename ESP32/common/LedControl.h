
#pragma once

#include <stdint.h>

void led_control_init();

void led_set_5000K_lf(uint32_t duty);
void led_set_5000K_hf(uint32_t duty);
void led_set_5000K(uint32_t duty);
uint32_t led_get_5000K();
float led_get_current_5000K();

void led_set_2700K_lf(uint32_t duty);
void led_set_2700K_hf(uint32_t duty);
void led_set_2700K(uint32_t duty);
uint32_t led_get_2700K();
float led_get_current_2700K();

void led_set_red_lf(uint32_t duty);
void led_set_red_hf(uint32_t duty);
void led_set_red(uint32_t duty);
uint32_t led_get_red();

void led_set_temp(float temp, float brightness);
