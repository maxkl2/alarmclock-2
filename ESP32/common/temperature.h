
#pragma once

void temp_init();
void temp_update();
void temp_loop();
void temp_get_2700K(float *temp_led, float *temp_ntc, float *current);
void temp_get_5000K(float *temp_led, float *temp_ntc, float *current);
