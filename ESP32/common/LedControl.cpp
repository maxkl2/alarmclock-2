
#include "LedControl.h"

#include <Arduino.h>

#define LED_LOG 0

#define LEDC_CH_5000K_LF 0 // LEDC timer 0
#define LEDC_CH_2700K_LF 1 // LEDC timer 0
#define LEDC_CH_RED_LF 2 // LEDC timer 1
#define LEDC_CH_5000K_HF 4 // LEDC timer 2
#define LEDC_CH_2700K_HF 5 // LEDC timer 2
#define LEDC_CH_RED_HF 6 // LEDC timer 3

#define PIN_5000K_LF 12
#define PIN_5000K_HF 13
#define PIN_2700K_LF 27
#define PIN_2700K_HF 14
#define PIN_RED_LF 25
#define PIN_RED_HF 26

#define LEDC_F_LF 500 // Hz
#define LEDC_F_HF 19500 // Hz

#define LEDC_BITS_LF 12
#define LEDC_BITS_HF 12

#define THRESH_LF_HF 500

#define CURRENT_5000K 0.5f // A
#define CURRENT_2700K 0.667f // A

#include "./lut_led_brightness.h"

static bool is_5000K_lf_enabled = true;
static bool is_2700K_lf_enabled = true;
static bool is_red_lf_enabled = true;

static uint32_t duty_5000K = 0;
static uint32_t duty_2700K = 0;
static uint32_t duty_red = 0;

void led_control_init() {
    // 500 Hz, 12 bit
    ledcAttachChannel(PIN_5000K_LF, LEDC_F_LF, LEDC_BITS_LF, LEDC_CH_5000K_LF);
    ledcAttachChannel(PIN_2700K_LF, LEDC_F_LF, LEDC_BITS_LF, LEDC_CH_2700K_LF);
    ledcAttachChannel(PIN_RED_LF, LEDC_F_LF, LEDC_BITS_LF, LEDC_CH_RED_LF);
    // 19.5 kHz, 12 bit
    ledcAttachChannel(PIN_5000K_HF, LEDC_F_HF, LEDC_BITS_HF, LEDC_CH_5000K_HF);
    ledcAttachChannel(PIN_2700K_HF, LEDC_F_HF, LEDC_BITS_HF, LEDC_CH_2700K_HF);
    ledcAttachChannel(PIN_RED_HF, LEDC_F_HF, LEDC_BITS_HF, LEDC_CH_RED_HF);
    // Initially off
    ledcWrite(PIN_5000K_LF, 0);
    ledcWrite(PIN_2700K_LF, 0);
    ledcWrite(PIN_RED_LF, 0);
    ledcWrite(PIN_5000K_HF, 0);
    ledcWrite(PIN_2700K_HF, 0);
    ledcWrite(PIN_RED_HF, 0);
}

// Scaling necessary since the AL8860 analog range is only roughly 0.3 - 2.5 V

uint32_t scale_duty_5000K_hf(uint32_t duty_lf) {
    // Parameters from linear regression: https://www.desmos.com/calculator/pgm4rfdlgl
    return roundf(0.616786f * (float) duty_lf + 452.857f);
}

uint32_t scale_duty_2700K_hf(uint32_t duty_lf) {
    // Parameters from linear regression: https://www.desmos.com/calculator/pgm4rfdlgl
    return roundf(0.637965f * (float) duty_lf + 324.709f);
}

uint32_t scale_duty_red_hf(uint32_t duty_lf) {
    // Parameters from linear regression: https://www.desmos.com/calculator/pgm4rfdlgl
    return roundf(0.711105f * (float) duty_lf + 200.872f);
}

void led_set_5000K_lf(uint32_t duty) {
    if (is_5000K_lf_enabled) {
        ledcWrite(PIN_5000K_LF, duty);
    } else {
        ledcAttachChannel(PIN_5000K_LF, LEDC_F_LF, LEDC_BITS_LF, LEDC_CH_5000K_LF);
        // Write does not take effect before ledcAttach()
        ledcWrite(PIN_5000K_LF, duty);

        ledcWrite(PIN_5000K_HF, 0);

        is_5000K_lf_enabled = true;
    }

#if LED_LOG
    Serial.print("5000K LF: ");
    Serial.println(duty);
#endif
}

void led_set_5000K_hf(uint32_t duty) {
    ledcWrite(PIN_5000K_HF, duty);

    if (is_5000K_lf_enabled) {
        // Puts pin into high-impedance
        pinMode(PIN_5000K_LF, INPUT);

        ledcDetach(PIN_5000K_LF);

        is_5000K_lf_enabled = false;
    }

#if LED_LOG
    Serial.print("5000K HF: ");
    Serial.println(duty);
#endif
}

void led_set_5000K(uint32_t duty) {
    duty_5000K = duty;
    if (duty < THRESH_LF_HF) {
        led_set_5000K_lf(duty);
    } else {
        led_set_5000K_hf(scale_duty_5000K_hf(duty));
    }
}

uint32_t led_get_5000K() {
    return duty_5000K;
}

float led_get_current_5000K() {
    return (float) duty_5000K / 4095.0f * CURRENT_5000K;
}

void led_set_2700K_lf(uint32_t duty) {
    if (is_2700K_lf_enabled) {
        ledcWrite(PIN_2700K_LF, duty);
    } else {
        ledcAttachChannel(PIN_2700K_LF, LEDC_F_LF, LEDC_BITS_LF, LEDC_CH_2700K_LF);
        // Write does not take effect before ledcAttach()
        ledcWrite(PIN_2700K_LF, duty);

        ledcWrite(PIN_2700K_HF, 0);

        is_2700K_lf_enabled = true;
    }

#if LED_LOG
    Serial.print("2700K LF: ");
    Serial.println(duty);
#endif
}

void led_set_2700K_hf(uint32_t duty) {
    ledcWrite(PIN_2700K_HF, duty);

    if (is_2700K_lf_enabled) {
        // Puts pin into high-impedance
        pinMode(PIN_2700K_LF, INPUT);

        ledcDetach(PIN_2700K_LF);

        is_2700K_lf_enabled = false;
    }

#if LED_LOG
    Serial.print("2700K HF: ");
    Serial.println(duty);
#endif
}

void led_set_2700K(uint32_t duty) {
    duty_2700K = duty;
    if (duty < THRESH_LF_HF) {
        led_set_2700K_lf(duty);
    } else {
        led_set_2700K_hf(scale_duty_2700K_hf(duty));
    }
}

uint32_t led_get_2700K() {
    return duty_2700K;
}

float led_get_current_2700K() {
    return (float) duty_2700K / 4095.0f * CURRENT_2700K;
}

void led_set_red_lf(uint32_t duty) {
    if (is_red_lf_enabled) {
        ledcWrite(PIN_RED_LF, duty);
    } else {
        ledcAttachChannel(PIN_RED_LF, LEDC_F_LF, LEDC_BITS_LF, LEDC_CH_RED_LF);
        // Write does not take effect before ledcAttach()
        ledcWrite(PIN_RED_LF, duty);

        ledcWrite(PIN_RED_HF, 0);

        is_red_lf_enabled = true;
    }

#if LED_LOG
    Serial.print("Red LF: ");
    Serial.println(duty);
#endif
}

void led_set_red_hf(uint32_t duty) {
    ledcWrite(PIN_RED_HF, duty);

    if (is_red_lf_enabled) {
        // Puts pin into high-impedance
        pinMode(PIN_RED_LF, INPUT);

        ledcDetach(PIN_RED_LF);

        is_red_lf_enabled = false;
    }

#if LED_LOG
    Serial.print("Red HF: ");
    Serial.println(duty);
#endif
}

void led_set_red(uint32_t duty) {
    duty_red = duty;
    if (duty < THRESH_LF_HF) {
        led_set_red_lf(duty);
    } else {
        led_set_red_hf(scale_duty_red_hf(duty));
    }
}

uint32_t led_get_red() {
    return duty_red;
}

void led_set_temp(float temp, float brightness) {
    float p_5000K, p_2700K, p_red;
    if (temp < 2700) {
        float p = (temp - 500) / (2700 - 500);
        if (p < 0) {
            p = 0;
        } else if (p > 1) {
            p = 1;
        }
        p_5000K = 0;
        p_2700K = p;
        p_red = 1.0f - p;
    } else {
        float p = (temp - 2700) / (5000 - 2700);
        if (p < 0) {
            p = 0;
        } else if (p > 1) {
            p = 1;
        }
        p_5000K = p;
        p_2700K = 1.0f - p;
        p_red = 0;
    }
    uint16_t max_duty = LED_BRIGHTNESS[(int) roundf(brightness * 255)];
#if LED_LOG
    Serial.print("Max. duty cycle: ");
    Serial.println(max_duty);
#endif
    led_set_5000K(p_5000K * max_duty);
    led_set_2700K(p_2700K * max_duty);
    led_set_red(p_red * max_duty);
}
