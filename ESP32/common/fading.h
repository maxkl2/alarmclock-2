
#pragma once

#include <stdint.h>


enum FadeStartMode {
    FADE_START_NORMAL,
    FADE_START_CURRENT,
    FADE_START_SMART
};

struct FadeInfo {
	FadeStartMode start_mode;
    uint32_t duration; // s, > 0
    float temp_start, temp_end; // K, 500 - 5000
    float brightness_start, brightness_end; // 0.0 - 1.0
};

void fading_task_start();

void fading_set_smooth(float temp, float brightness);
void fading_schedule(int32_t delay, FadeInfo *fade);
