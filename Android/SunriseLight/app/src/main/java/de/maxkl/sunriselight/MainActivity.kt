package de.maxkl.sunriselight

import android.Manifest
import android.app.Activity
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts.RequestMultiplePermissions
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import de.maxkl.sunriselight.ui.theme.SunriseLightTheme
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : ComponentActivity() {
    companion object {
        private const val TAG = "MainActivity"
    }

    private var requestLocationPermission: (() -> Unit)? = null
    private var requestBluetoothPermissions: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val requestPermissionLauncher = registerForActivityResult(RequestMultiplePermissions()) { permissions ->
            val containsPermanentDenial = permissions.any {
                it.value == false && !ActivityCompat.shouldShowRequestPermissionRationale(this, it.key)
            }
            val containsDenial = permissions.any { it.value == false }
            val allGranted = permissions.all { it.value == true }
            when {
                containsPermanentDenial -> {
                    Log.e(TAG, "One or more permissions permanently denied")
                    // TODO: Handle permanent denial (e.g., show AlertDialog with justification)
                    // Note: The user will need to navigate to App Settings and manually grant
                    // permissions that were permanently denied:
                    // Launch an Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS) and set the Intent's data to a specific URI which describes the app's package name, e.g. package:com.punchthrough.lightblueandroid
                }
                containsDenial -> {
                    Log.d(TAG, "One or more permissions denied")
                    requestRelevantRuntimePermissions()
                }
                allGranted && hasRequiredRuntimePermissions() -> {
                    Log.d(TAG, "All permissions granted")
//                    startBleScan() <- TODO
                }
                else -> {
                    // Unexpected scenario encountered when handling permissions
                    Log.d(TAG, "Unexpected case in permission request callback: allGranted=$allGranted, hasRequiredRuntimePermissions=${hasRequiredRuntimePermissions()}")
                    recreate()
                }
            }
        }

        setContent {
            var showLocationPermissionDialog by remember { mutableStateOf(false) }
            var showBluetoothPermissionDialog by remember { mutableStateOf(false) }

            requestLocationPermission = { showLocationPermissionDialog = true }
            requestBluetoothPermissions = { showBluetoothPermissionDialog = true }

            val coroutineScope = rememberCoroutineScope()

            SunriseLightTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                    MainLayout()
                }

                PermissionRequestDialog(
                    title = "Location permission required",
                    text = "Starting from Android M (6.0), the system requires apps to be granted location access in order to scan for BLE devices.",
                    show = showLocationPermissionDialog,
                    onShowChange = { showLocationPermissionDialog = it },
                    onRequestPermission = {
                        requestPermissionLauncher.launch(arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ))
                    }
                )

                PermissionRequestDialog(
                    title = "Bluetooth permissions required",
                    text = "Starting from Android 12, the system requires apps to be granted Bluetooth access in order to scan for and connect to BLE devices.",
                    show = showBluetoothPermissionDialog,
                    onShowChange = { showBluetoothPermissionDialog = it },
                    onRequestPermission = {
                        requestPermissionLauncher.launch(arrayOf(
                            Manifest.permission.BLUETOOTH_SCAN,
                            Manifest.permission.BLUETOOTH_CONNECT
                        ))
                    }
                )
            }
        }
    }

    private fun ensurePermissions(): Boolean {
        if (hasRequiredRuntimePermissions()) {
            return true
        }

        requestRelevantRuntimePermissions()

        return false
    }

    private fun Activity.requestRelevantRuntimePermissions() {
        if (hasRequiredRuntimePermissions()) { return }
        when {
            Build.VERSION.SDK_INT < Build.VERSION_CODES.S -> {
                requestLocationPermission!!()
            }
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
                requestBluetoothPermissions!!()
            }
        }
    }
}

/*
 * TODO:
 *   - BLE communication (dedicated bg thread?)
 *   - automatic sync
 *   - persistent storage of settings, sync state
 *   - notification when sync failed or
 *   - manual setting of brightness & color temperature
 *   - enable/disable sunrise (necessary?)
 *   - enable/disable app
 *   - sunset (settable duration, start on click)
 *   - strings.xml & localization
 *
 * Sync when:
 * - phone booted
 * - alarm changed (-> broadcast)
 * - alarm went off (-> broadcast)
 * - UI button pressed
 * - ESP32 was not reachable before (retry every x minutes)
 * - ESP32 restarted (request by ESP32)
 * - periodically in case ESP32 clock freq is off (request by ESP32?)
 *
 * Store persistently:
 * - Last successful sync (sync time, alarm time)
 * - whether last sync try was successful
 */

fun iconForConnectionState(connectionState: ConnectionState): Pair<ImageVector, String> {
    return when (connectionState) {
        ConnectionState.Unknown -> Pair(Icons.Rounded.QuestionMark, "Unknown connection state")
        ConnectionState.MissingPermissions -> Pair(Icons.Rounded.Error, "Missing permissions")
        ConnectionState.BtDisabled -> Pair(Icons.Rounded.BluetoothDisabled, "Bluetooth disabled")
        ConnectionState.BtScanning -> Pair(Icons.Rounded.BluetoothSearching, "Scanning for device...")
        ConnectionState.BtNotFound -> Pair(Icons.Rounded.Warning, "No device found")
        ConnectionState.BtConnecting -> Pair(Icons.Rounded.Pending, "Connecting...")
        ConnectionState.BtConnected -> Pair(Icons.Rounded.BluetoothConnected, "Connected")
        ConnectionState.BtConnectError -> Pair(Icons.Rounded.Warning, "Bluetooth connection failed")
        ConnectionState.Syncing -> Pair(Icons.Rounded.Sync, "Syncing...")
        ConnectionState.SyncError -> Pair(Icons.Rounded.SyncProblem, "Sync error")
    }
}

// TODO: split into sub-composables
@Composable
fun MainLayout(
    myViewModel: ISunriseLightViewModel = viewModel<SunriseLightViewModel>()
) {
    // TODO: use collectAsStateWithLifecycle()
    val uiState by myViewModel.uiState.collectAsState()

    val (icon, iconDesc) = iconForConnectionState(uiState.connectionState)

    val sdf = SimpleDateFormat.getDateTimeInstance()
    val alarmTime = uiState.nextAlarmTime
    val nextAlarmString = if (alarmTime != null) sdf.format(alarmTime) else "no alarm"

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Sunrise Light") },
                actions = {
//                    IconButton(onClick = { /*TODO*/ }) {
//                        Icon(
//                            Icons.Rounded.Refresh,
//                            tint = LocalContentColor.current,
//                            contentDescription = "Retry connection"
//                        )
//                    }
                    IconButton(onClick = { /*TODO*/ }) {
                        Icon(
                            icon,
                            contentDescription = iconDesc,
                            tint = LocalContentColor.current
                        )
                    }
                    // Rotating? https://stackoverflow.com/q/68381193/3593126
                }
            )
        },
        content = { contentPadding ->
            Column(
                modifier = Modifier
                    .padding(contentPadding)
                    .verticalScroll(rememberScrollState())
            ) {
                val padding = Modifier.padding(horizontal = 10.dp, vertical = 2.dp)
                Text(text = "Next alarm: $nextAlarmString", modifier = padding)
                Text(text = "Last sync'ed at: <sync time>", modifier = padding)
                Text(text = "Last sync'ed alarm: <last alarm>", modifier = padding)
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.colorTemp,
                        onValueChange = { myViewModel.updateColorTemp(it) },
                        label = { Text("Color temperature (K)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.brightness,
                        onValueChange = { myViewModel.updateBrightness(it) },
                        label = { Text("Brightness (%)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Row(modifier = padding) {
                    Spacer(modifier = Modifier.weight(1f))
                    Button(
                        enabled = uiState.connectionState.canSync(),
                        onClick = { myViewModel.syncManual() }
                    ) { Text("Set manual") }
                }
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.sunriseDelay,
                        onValueChange = { myViewModel.updateSunriseDelay(it) },
                        label = { Text("Delay (s)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.weight(1f)
                    )
                    Button(onClick = { myViewModel.calcSunriseDelayFromAlarm() }) {
                        Text("From alarm")
                    }
                }
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.sunriseDelayOffset,
                        onValueChange = { myViewModel.updateSunriseDelayOffset(it) },
                        label = { Text("Offset (s)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.sunriseDuration,
                        onValueChange = { myViewModel.updateSunriseDuration(it) },
                        label = { Text("Duration (s)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.sunriseColorTemp,
                        onValueChange = { myViewModel.updateSunriseColorTemp(it) },
                        label = { Text("Color temperature (K)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Row(modifier = padding) {
                    TextField(
                        value = myViewModel.sunriseBrightness,
                        onValueChange = { myViewModel.updateSunriseBrightness(it) },
                        label = { Text("Brightness (%)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Row(modifier = padding) {
                    Spacer(modifier = Modifier.weight(1f))
                    Button(
                        enabled = uiState.connectionState.canSync(),
                        onClick = { myViewModel.syncSunrise() }
                    ) { Text("Set sunrise") }
                }
            }
        }
    )
}

@Composable
fun PermissionRequestDialog(title: String, text: String, show: Boolean, onShowChange: (Boolean) -> Unit, onRequestPermission: () -> Unit) {
    if (show) {
        AlertDialog(
            onDismissRequest = { /* Do nothing so that tapping outside the dialog won't close it */ },
            title = {
                Text(text = title)
            },
            text = {
                Text(text = text)
            },
            confirmButton = {
                Button(
                    modifier = Modifier.fillMaxWidth(),
                    onClick = {
                        onShowChange(false)
                        onRequestPermission()
                    }
                ) {
                    Text(stringResource(android.R.string.ok))
                }
            }
        )
    }
}

private class PreviewSunriseLightViewModel(
    uiState: SunriseLightUiState,
    override val colorTemp: String = "2700",
    override val brightness: String = "50",
    override val sunriseDelay: String = "900",
    override val sunriseDelayOffset: String = "-600",
    override val sunriseDuration: String = "1200",
    override val sunriseColorTemp: String = "4000",
    override val sunriseBrightness: String = "80"
) : ISunriseLightViewModel {
    override val uiState: StateFlow<SunriseLightUiState> = MutableStateFlow(uiState).asStateFlow()

    override fun updateColorTemp(it: String) {}
    override fun updateBrightness(it: String) {}
    override fun updateSunriseDelay(it: String) {}
    override fun updateSunriseDelayOffset(it: String) {}
    override fun calcSunriseDelayFromAlarm() {}
    override fun updateSunriseDuration(it: String) {}
    override fun updateSunriseColorTemp(it: String) {}
    override fun updateSunriseBrightness(it: String) {}
    override fun syncManual() {}
    override fun syncSunrise() {}
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    showBackground = true,
    name = "Dark Mode"
)
@Composable
fun DefaultPreview() {
    val viewModel = PreviewSunriseLightViewModel(
        SunriseLightUiState(
            connectionState = ConnectionState.BtDisabled,
            nextAlarmTime = Date()
        )
    )
    SunriseLightTheme {
        Surface {
            MainLayout(viewModel)
        }
    }
}

@Preview
@Composable
fun PermissionRequestDialogPreview() {
    SunriseLightTheme {
        PermissionRequestDialog(
            title = "Location permission required",
            text = "Starting from Android M (6.0), the system requires apps to be granted location access in order to scan for BLE devices.",
            show = true, onShowChange = {}, onRequestPermission = {}
        )
    }
}
