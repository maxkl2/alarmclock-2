package de.maxkl.sunriselight

import android.app.AlarmManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

private const val TAG = "AlarmClockChangedReceiv"

class AlarmClockChangedReceiver : BroadcastReceiver() {
    private val scope = CoroutineScope(SupervisorJob())

    override fun onReceive(context: Context, intent: Intent) {
        assert(intent.action == AlarmManager.ACTION_NEXT_ALARM_CLOCK_CHANGED)

        val pendingResult = goAsync()

        scope.launch(Dispatchers.Default) {
            try {
                val sdf = SimpleDateFormat.getDateTimeInstance()
                val alarmTime = AlarmUtils.getNextAlarmTime(context)
                val text = if (alarmTime != null) sdf.format(alarmTime) else "no alarm"

                StringBuilder().apply {
                    append("Alarm changed: ${text}\n")
                    toString().also { log ->
                        Log.d(TAG, log)
                    }
                }
            } finally {
                pendingResult.finish()
            }
        }
    }
}