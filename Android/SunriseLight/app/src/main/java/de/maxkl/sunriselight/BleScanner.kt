package de.maxkl.sunriselight

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.*
import android.os.ParcelUuid
import android.util.Log
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

data class ScanningException(val errorCode: Int): Exception()

@SuppressLint("MissingPermission")
class BleScanner(
    private val bluetoothAdapter: BluetoothAdapter
) {
    companion object {
        private const val TAG = "BleScanner"
    }

    private val bluetoothLeScanner: BluetoothLeScanner by lazy {
        bluetoothAdapter.bluetoothLeScanner
            ?: throw NullPointerException("Bluetooth not initialized")
    }

    /**
     * Starts scanning for an advertising Sunrise Light. Returns the first found device
     */
    suspend fun searchForSunriseLight(): BluetoothDevice = suspendCancellableCoroutine { continuation ->
        var stopped = false
        val callback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                Log.d(TAG, "Scan result: ${if (result != null) result.device.address else "null"} (stopped=$stopped)")

                // onScanResult() may still be called a few times with cached results even after stopScan() was called
                if (!stopped) {
                    result?.let {
                        stopped = true
                        bluetoothLeScanner.stopScan(this)
                        continuation.resume(it.device)
                    }
                }
            }

            override fun onScanFailed(errorCode: Int) {
                continuation.resumeWithException(ScanningException(errorCode))
            }
        }
        continuation.invokeOnCancellation {
            stopped = true
            bluetoothLeScanner.stopScan(callback)
        }

        val scanSettings = ScanSettings.Builder()
            .setReportDelay(0) // Set to 0 to be notified of scan results immediately.
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()

        val scanFilters = listOf(
            ScanFilter.Builder()
                .setServiceUuid(ParcelUuid(BleUuids.UUID_SUNRISE_LIGHT_SERVICE))
                .build()
        )

        bluetoothLeScanner.startScan(
            scanFilters,
            scanSettings,
            callback
        )
    }
}