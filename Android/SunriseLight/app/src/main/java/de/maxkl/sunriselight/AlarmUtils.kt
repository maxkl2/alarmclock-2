package de.maxkl.sunriselight

import android.app.AlarmManager
import android.content.Context
import java.util.Date

class AlarmUtils {
    companion object {
        fun getNextAlarmTime(context: Context): Date? {
            val alarmManager = (context.getSystemService(Context.ALARM_SERVICE) as? AlarmManager)!!
            val nextAlarm = alarmManager.nextAlarmClock
            return if (nextAlarm != null) Date(nextAlarm.triggerTime) else null
        }
    }
}