package de.maxkl.sunriselight

import java.util.*

class BleUuids {
    companion object {
        val UUID_SUNRISE_LIGHT_SERVICE: UUID by lazy { UUID.fromString("c2319101-1e43-4960-a57b-cb2e3bc6893d") }
        // Characteristic "Sync Request": Indication
        val UUID_SYNC_REQUEST_CHARACTERISTIC: UUID by lazy { UUID.fromString("89ea188e-9a04-4e35-b349-afacad50dce2") }
        // Characteristic "Manual": U16 colorTemp, U8 brightness
        val UUID_MANUAL_CHARACTERISTIC: UUID by lazy { UUID.fromString("93e4ddcc-d0ff-45bf-883d-4dd674ec83a7") }
        // Characteristic "Sunrise": I24 delay, U16 duration, U16 colorTemp, U8 brightness
        val UUID_SUNRISE_CHARACTERISTIC: UUID by lazy { UUID.fromString("52bf3022-e0a1-4cc8-a72f-99ae45fc8cb0") }
        // Characteristic "Sunset": I24 delay, U16 duration, U16 colorTemp, U8 brightness
        val UUID_SUNSET_CHARACTERISTIC: UUID by lazy { UUID.fromString("736e6e70-1742-4a9b-95e4-8ecebf399fdb") }
    }
}