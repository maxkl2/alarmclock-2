package de.maxkl.sunriselight

import android.app.Application
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import no.nordicsemi.android.ble.exception.RequestFailedException
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.math.roundToInt

enum class ConnectionState {
    Unknown,
    MissingPermissions,
    BtDisabled,
    BtScanning,
    BtNotFound,
    BtConnecting,
    BtConnected,
    BtConnectError,
    Syncing,
    SyncError
}

fun ConnectionState.canSync(): Boolean = when (this) {
    ConnectionState.BtConnected,
    ConnectionState.Syncing,
    ConnectionState.SyncError -> true
    else -> false
}

data class SunriseLightUiState(
    val connectionState: ConnectionState = ConnectionState.Unknown,
    val nextAlarmTime: Date? = null
)

interface ISunriseLightViewModel {
    val uiState: StateFlow<SunriseLightUiState>

    val colorTemp: String
    val brightness: String
    val sunriseDelay: String
    val sunriseDelayOffset: String
    val sunriseDuration: String
    val sunriseColorTemp: String
    val sunriseBrightness: String

    fun updateColorTemp(it: String)

    fun updateBrightness(it: String)

    fun updateSunriseDelay(it: String)

    fun updateSunriseDelayOffset(it: String)

    fun calcSunriseDelayFromAlarm()

    fun updateSunriseDuration(it: String)

    fun updateSunriseColorTemp(it: String)

    fun updateSunriseBrightness(it: String)

    fun syncManual()

    fun syncSunrise()
}

@OptIn(FlowPreview::class)
class SunriseLightViewModel(application: Application) : ISunriseLightViewModel, AndroidViewModel(application) {
    companion object {
        private const val TAG = "SunriseLightViewModel"
    }

    private val _uiState = MutableStateFlow(SunriseLightUiState())
    override val uiState = _uiState.asStateFlow()

    override var colorTemp by mutableStateOf("2700")
        private set
    override var brightness by mutableStateOf("50")
        private set
    override var sunriseDelay by mutableStateOf("900")
        private set
    override var sunriseDelayOffset by mutableStateOf("-600")
        private set
    override var sunriseDuration by mutableStateOf("1200")
        private set
    override var sunriseColorTemp by mutableStateOf("4000")
        private set
    override var sunriseBrightness by mutableStateOf("80")
        private set

    private val repo = SunriseLightRepository(application)

    private val isSyncing = MutableStateFlow(false)

    override fun updateColorTemp(it: String) {
        colorTemp = it
    }

    override fun updateBrightness(it: String) {
        brightness = it
    }

    override fun updateSunriseDelay(it: String) {
        sunriseDelay = it
    }

    override fun updateSunriseDelayOffset(it: String) {
        sunriseDelayOffset = it
    }

    override fun calcSunriseDelayFromAlarm() {
        val nextAlarmTime = _uiState.value.nextAlarmTime
        if (nextAlarmTime != null) {
            sunriseDelay = Duration.between(Instant.now(), nextAlarmTime.toInstant()).seconds.toString()
        }
    }

    override fun updateSunriseDuration(it: String) {
        sunriseDuration = it
    }

    override fun updateSunriseColorTemp(it: String) {
        sunriseColorTemp = it
    }

    override fun updateSunriseBrightness(it: String) {
        sunriseBrightness = it
    }

    override fun syncManual() {
        // TODO: report errors to view (use isError of TextField)
        val colorTemp = (colorTemp.toFloatOrNull() ?: 2700f).coerceIn(500f, 5000f).roundToInt().toUShort()
        val brightness = (brightness.toFloatOrNull() ?: 50f).coerceIn(0f, 100f).times(255f / 100f).roundToInt().toUByte()
        viewModelScope.launch {
            isSyncing.value = true

            try {
                repo.setManual(colorTemp, brightness)
            } catch (e: RequestFailedException) {
                Log.e(TAG, "Failed to sync", e)
                // TODO: indicate error in UI
            }

            isSyncing.value = false
        }
    }

    override fun syncSunrise() {
        // TODO: report errors to view (use isError of TextField)
        val sunriseDelay = (sunriseDelay.toFloatOrNull() ?: 0f)
        val sunriseDelayOffset = (sunriseDelayOffset.toFloatOrNull() ?: 0f)
        val delayTotal = sunriseDelay + sunriseDelayOffset
        val sunriseDuration = (sunriseDuration.toFloatOrNull() ?: 1200f).coerceAtLeast(0f)
        val sunriseColorTemp = (sunriseColorTemp.toFloatOrNull() ?: 4000f).coerceIn(500f, 5000f)
        val sunriseBrightness = (sunriseBrightness.toFloatOrNull() ?: 80f).coerceIn(0f, 100f).times(255f / 100f)
        viewModelScope.launch {
            isSyncing.value = true

            try {
                repo.setSunrise(
                    delayTotal.roundToInt(),
                    sunriseDuration.roundToInt().toUShort(),
                    sunriseColorTemp.roundToInt().toUShort(),
                    sunriseBrightness.roundToInt().toUByte()
                )
            } catch (e: RequestFailedException) {
                Log.e(TAG, "Failed to sync", e)
                // TODO: indicate error in UI
            }

            isSyncing.value = false
        }
    }

    init {
        _uiState.update {
            SunriseLightUiState(
                nextAlarmTime = AlarmUtils.getNextAlarmTime(application)
            )
        }

        viewModelScope.launch {
            repo.bleState.combine(isSyncing) { bleState, isSyncing ->
                when (bleState) {
                    BleState.NotConnected -> ConnectionState.Unknown
                    BleState.Scanning -> ConnectionState.BtScanning
                    BleState.NotFound -> ConnectionState.BtNotFound
                    BleState.Connecting -> ConnectionState.BtConnecting
                    BleState.ConnectError -> ConnectionState.BtConnectError
                    BleState.Connected -> if (isSyncing) ConnectionState.Syncing else ConnectionState.BtConnected
                }
            }.collect { connectionState ->
                Log.d(TAG, "Connection state: ${connectionState.name}")
                _uiState.update { uiState ->
                    uiState.copy(connectionState = connectionState)
                }
            }
        }

        viewModelScope.launch {
            repo.scanAndConnect()
            repo.bleState.map { when (it) {
                BleState.NotConnected,
                BleState.NotFound,
                BleState.ConnectError -> true
                else -> false
            } }.debounce(5000).collect { shouldReconnect ->
                if (shouldReconnect) {
                    repo.scanAndConnect()
                }
            }
        }
    }
}