package de.maxkl.sunriselight

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.content.Context
import android.util.Log
import no.nordicsemi.android.ble.BleManager
import no.nordicsemi.android.ble.ktx.getCharacteristic
import no.nordicsemi.android.ble.ktx.suspend

class SunriseLightBleManager(
    context: Context
) : BleManager(context) {
    companion object {
        private const val TAG = "SunriseLightBleManager"
    }

    override fun getMinLogPriority(): Int = Log.VERBOSE

    override fun log(priority: Int, message: String) {
        Log.println(priority, TAG, message)
    }

    private var manualCharacteristic: BluetoothGattCharacteristic? = null
    private var sunriseCharacteristic: BluetoothGattCharacteristic? = null
    private var sunsetCharacteristic: BluetoothGattCharacteristic? = null

    override fun isRequiredServiceSupported(gatt: BluetoothGatt): Boolean {
        val sunriseLightService = gatt.getService(BleUuids.UUID_SUNRISE_LIGHT_SERVICE)
            ?: return false

        manualCharacteristic = sunriseLightService.getCharacteristic(BleUuids.UUID_MANUAL_CHARACTERISTIC, BluetoothGattCharacteristic.PROPERTY_WRITE)
            ?: return false
        sunriseCharacteristic = sunriseLightService.getCharacteristic(BleUuids.UUID_SUNRISE_CHARACTERISTIC, BluetoothGattCharacteristic.PROPERTY_WRITE)
            ?: return false
        sunsetCharacteristic = sunriseLightService.getCharacteristic(BleUuids.UUID_SUNSET_CHARACTERISTIC, BluetoothGattCharacteristic.PROPERTY_WRITE)
            ?: return false

        return true
    }

    override fun initialize() {
        // TODO: subscribe to sync request
    }

    override fun onServicesInvalidated() {
        manualCharacteristic = null
        sunriseCharacteristic = null
        sunsetCharacteristic = null
    }

    suspend fun connectDevice(device: BluetoothDevice) {
        connect(device)
            .retry(4, 300)
            .useAutoConnect(false)
            .timeout(100_000)
            .suspend()
    }

    fun release() {
        // TODO: cancel coroutines?
        cancelQueue()
        disconnect()
            .enqueue()
    }

    suspend fun setManual(colorTemp: UShort, brightness: UByte) {
        val data = byteArrayOf(
            (colorTemp.toInt() shr 8).toByte(),
            colorTemp.toByte(),
            brightness.toByte()
        )

        writeCharacteristic(
            manualCharacteristic,
            data,
            BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        ).suspend()
    }

    suspend fun setSunrise(delay: Int, duration: UShort, colorTemp: UShort, brightness: UByte) {
        val data = byteArrayOf(
            (delay shr 16).toByte(),
            (delay shr 8).toByte(),
            delay.toByte(),
            (duration.toInt() shr 8).toByte(),
            duration.toByte(),
            (colorTemp.toInt() shr 8).toByte(),
            colorTemp.toByte(),
            brightness.toByte()
        )

        writeCharacteristic(
            sunriseCharacteristic,
            data,
            BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        ).suspend()
    }

    suspend fun setSunset(delay: Int, duration: UShort, colorTemp: UShort, brightness: UByte) {
        val data = byteArrayOf(
            (delay shr 16).toByte(),
            (delay shr 8).toByte(),
            delay.toByte(),
            (duration.toInt() shr 8).toByte(),
            duration.toByte(),
            (colorTemp.toInt() shr 8).toByte(),
            colorTemp.toByte(),
            brightness.toByte()
        )

        writeCharacteristic(
            sunsetCharacteristic,
            data,
            BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        ).suspend()
    }
}