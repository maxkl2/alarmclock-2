package de.maxkl.sunriselight

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import no.nordicsemi.android.ble.ktx.state.ConnectionState
import no.nordicsemi.android.ble.ktx.stateAsFlow

enum class BleState {
    NotConnected,
    Scanning,
    NotFound,
    Connecting,
    ConnectError,
    Connected
}

private enum class ScanState {
    NotScanning,
    Scanning,
    NotFound,
    Found
}

// TODO: unify repository and BleManager?

@SuppressLint("MissingPermission")
class SunriseLightRepository(
    private val context: Context
    // TODO: use app-oriented scope that survives Activity restarts (viewModelScope not enough?)
//    // This could be CoroutineScope(SupervisorJob() + Dispatchers.Default).
//    private val externalScope: CoroutineScope
) {
    companion object {
        private const val TAG = "SunriseLightRepository"
    }

    // TODO: use external or other provided scope?
    private val scope = CoroutineScope(Dispatchers.IO)

    private val bluetoothAdapter: BluetoothAdapter by lazy {
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }

    private val bleScanner: BleScanner by lazy { BleScanner(bluetoothAdapter) }

    private val bleManager = SunriseLightBleManager(context)

    private val connectionState = bleManager.stateAsFlow()
    private val scanState = MutableStateFlow(ScanState.NotScanning)
    val bleState = scanState.combine(connectionState) { scanState, connectionState ->
        when (connectionState) {
            ConnectionState.Connecting,
            ConnectionState.Initializing -> BleState.Connecting
            ConnectionState.Ready -> BleState.Connected
            ConnectionState.Disconnecting -> BleState.NotConnected
            is ConnectionState.Disconnected -> when (scanState) {
                ScanState.NotScanning -> when (connectionState.reason) {
                    ConnectionState.Disconnected.Reason.SUCCESS,
                    ConnectionState.Disconnected.Reason.TERMINATE_LOCAL_HOST,
                    ConnectionState.Disconnected.Reason.TERMINATE_PEER_USER,
                    ConnectionState.Disconnected.Reason.LINK_LOSS -> BleState.NotConnected
                    ConnectionState.Disconnected.Reason.UNKNOWN,
                    ConnectionState.Disconnected.Reason.NOT_SUPPORTED,
                    ConnectionState.Disconnected.Reason.CANCELLED,
                    ConnectionState.Disconnected.Reason.TIMEOUT -> BleState.ConnectError
                }
                ScanState.Scanning -> BleState.Scanning
                ScanState.NotFound -> BleState.NotFound
                ScanState.Found -> BleState.Connecting
            }
        }
    }.stateIn(scope, SharingStarted.Lazily, BleState.NotConnected)

    suspend fun setManual(colorTemp: UShort, brightness: UByte) {
        withContext(Dispatchers.IO) {
            bleManager.setManual(colorTemp, brightness)
        }
    }

    suspend fun getManual() {
        TODO()
    }

    suspend fun setSunrise(delay: Int, duration: UShort, colorTemp: UShort, brightness: UByte) {
        withContext(Dispatchers.IO) {
            bleManager.setSunrise(delay, duration, colorTemp, brightness)
        }
    }

    suspend fun setSunset(delay: Int, duration: UShort, colorTemp: UShort, brightness: UByte) {
        withContext(Dispatchers.IO) {
            bleManager.setSunset(delay, duration, colorTemp, brightness)
        }
    }

    suspend fun scanAndConnect() {
        // TODO already connected?

        Log.d(TAG, "Scanning...")

        withContext(Dispatchers.IO) {
            val device = try {
                scanState.value = ScanState.Scanning

                bleScanner.searchForSunriseLight()
            } catch (e: ScanningException) {
                Log.e(TAG, "BLE scan failed: ${e.errorCode}", e)

                scanState.value = ScanState.NotFound

                throw e
            }

            Log.i(TAG, "Found BLE device! Name: ${device.name ?: "Unnamed"}, address: ${device.address}")

            scanState.value = ScanState.Found

            try {
                bleManager.connectDevice(device)
            } catch (e: Exception) {
                Log.e(TAG, "BLE connection failed", e)
                throw e
            } finally {
                scanState.value = ScanState.NotScanning
            }
        }
    }

    fun disconnect() {
//        scope.cancel() // TODO: needed?
        bleManager.release()
    }
}